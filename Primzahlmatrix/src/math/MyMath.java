package math;

public class MyMath {

	public static boolean isPrim(long zahl) {
		if(zahl <= 1) {
			return false;
		}
		if(zahl == 2) {
			return true;
		}
		long sqrt = (long) (Math.sqrt(zahl) + 0.5);
		for(long i = 2; i <= sqrt; i++) {
			if(zahl % i == 0) {
				return false;
			}
		}
		return true;
	}
}
