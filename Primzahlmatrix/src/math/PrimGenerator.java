package math;

public class PrimGenerator {
	
	public static boolean[] generatePrimNumbers() {
		return generatePrimNumbers(1);
	}
	
	public static boolean[] generatePrimNumbers(long startnumber) {
		return generatePrimNumbers(startnumber,10000);
	}
	
	public static boolean[] generatePrimNumbers(long startnumber, int size) {
		boolean result[] = new boolean[size];
		long zahl = startnumber;
		int pos = 0;
		if(startnumber > Long.MAX_VALUE - size) {
			pos = (int) (Long.MAX_VALUE - startnumber);
		}
		int mod30 = (int) (zahl % 30);
		while(zahl <= 5 && pos < size) {				//Sonderbehandlung f�r 2, 3 und 5
			result[pos] = MyMath.isPrim(zahl);
			zahl++;
			pos++;
			mod30++;
		}
		while(pos < size) {
			switch(mod30) {								// Nur die relevanten Restklassen werden durchsucht
				case 1:
				case 7:
				case 11:
				case 13:
				case 17:
				case 19:
				case 23:
					result[pos] = MyMath.isPrim(zahl);
					mod30++;
					break;
				case 29:
					result[pos] = MyMath.isPrim(zahl);
					mod30 = 0;
					break;
				default:
					result[pos] = false;
					mod30++;
					break;
			}
			zahl++;
			pos++;
		}
		return result;
	}
	
}
