package primmatrix;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import math.PrimGenerator;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class PrimmatrixGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPanel primPanel;
	private JTextField textSize;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrimmatrixGUI frame = new PrimmatrixGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrimmatrixGUI() {
		long startwert;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1800, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Startwert");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(textField);
		textField.setColumns(10);
		JLabel labelSize = new JLabel("Groesse");
		panel.add(labelSize);
		
		textSize = new JTextField();
		panel.add(textSize);
		textSize.setHorizontalAlignment(SwingConstants.CENTER);
		textSize.setColumns(10);
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				primPanel.removeAll();
				long startwert = Long.parseLong(textField.getText());
				int size = Integer.parseInt(textSize.getText());
				startwert = (startwert/size)*size;
				boolean[] zahlen = PrimGenerator.generatePrimNumbers(startwert,size);
				for(int i = 0; i < size; i++) {
					JPanel tmp = new JPanel();
					tmp.setSize(5, 5);
					if(zahlen[i]) {
						tmp.setBackground(Color.RED);
					}else{
						tmp.setBackground(Color.WHITE);
					}
					tmp.setToolTipText(Long.toString(startwert + i));
					primPanel.add(tmp);
				}
				setTitle("Primzahlmatrix: " + startwert + " - " + (startwert + size - 1));
				primPanel.updateUI();
			}
			
		});
		
		btnNewButton.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(btnNewButton);
		
		primPanel = new JPanel();
		contentPane.add(primPanel, BorderLayout.WEST);
		primPanel.setLayout(new GridLayout(0, 100, 0, 0));
		
		startwert = 1;
		setTitle("Primzahlmatrix: " + startwert + " - " + (startwert + 9999));
		boolean[] zahlen = PrimGenerator.generatePrimNumbers();
		for(int i = 0; i < 10000; i++) {
			JPanel tmp = new JPanel();
			tmp.setSize(5, 5);
			if(zahlen[i]) {
				tmp.setBackground(Color.RED);
			}else{
				tmp.setBackground(Color.WHITE);
			}
			tmp.setToolTipText(Long.toString(startwert + i));
			primPanel.add(tmp);
		}
	}

}
