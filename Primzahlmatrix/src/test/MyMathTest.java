package test;

public class MyMathTest {
	
	public static void main(String[] args) {
		System.out.println("1: " + math.MyMath.isPrim(1));
		System.out.println("2: " + math.MyMath.isPrim(2));
		System.out.println("5001: " + math.MyMath.isPrim(5001));
		System.out.println("9973: " + math.MyMath.isPrim(9973));
		System.out.println("10000: " + math.MyMath.isPrim(10000));

	}

}
