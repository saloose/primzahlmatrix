package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PrimGeneratorTest {

	@Test
	void generatePrimTestNormalfall() {
		boolean testarray[] = math.PrimGenerator.generatePrimNumbers();
		boolean testresult = true;
		for(int i = 0; i < 10000; i++) {
			if(testarray[i] != math.MyMath.isPrim(i+1)) {
				testresult = false;
				break;
			}
		}
		assertTrue(testresult);
	}
	
	@Test
	void generatePrimTestArrayLength() {
		boolean testarray[] = math.PrimGenerator.generatePrimNumbers();
		assertEquals(testarray.length, 10000);
		
	}

}
